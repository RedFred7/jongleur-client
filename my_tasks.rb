# frozen_string_literal: true

class A < Jongleur::WorkerTask
  @desc = 'this is task A'
  def execute
    sleep 1
    STDOUT.puts 'A is running... '
  end
end

class B < Jongleur::WorkerTask
  @desc = 'this is task B'
  def execute(colour: 'red', size: 100  )
    sleep 3
    STDOUT.puts "B is running... with #{colour} and #{size}"
  end
end

class C < Jongleur::WorkerTask
  @desc = 'this is task C'
  def execute
    sleep 1
    STDOUT.puts 'C is running...'
  end
end

class D < Jongleur::WorkerTask
  @desc = 'this is task D'
  def execute
    sleep 1
    STDOUT.puts 'D is running...'
  end
end

class E < Jongleur::WorkerTask
  @desc = 'this is task E'
  def execute
    sleep 1
    STDOUT.puts 'E is running...'
  end
end

class F < Jongleur::WorkerTask
  @desc = 'this is task F'
  def execute
    sleep 1
    STDOUT.puts 'F is running...'
  end
end
