require 'jongleur'
require_relative 'my_tasks'
include Jongleur

my_graph = {
  A: %i[B C D],
  C: [:D],
  B: [],
  D: []
}

API.add_task_graph my_graph
API.print_graph('/tmp')

API.run
